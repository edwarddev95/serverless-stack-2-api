import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.tableName,
    // 'Key' defines the partition key and sort key of the item to be updated
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'noteId': path parameter
    Key: {
      userId: event.requestContext.identity.cognitoIdentityId,
      itemId: event.pathParameters.id
    },
    // 'UpdateExpression' defines the attributes to be updated
    // 'ExpressionAttributeValues' defines the value in the update expression
    UpdateExpression: "SET itemType = :itemType, designer = :designer, category = :category, itemCondition = :itemCondition, receipt = :receipt, tags = :tags, images = :images, submissionId = :submissionId, submitted = :submitted, submissionDate = :submissionDate",
    ExpressionAttributeValues: {
      ":itemType": data.itemType ? data.itemType : null,
      ":designer": data.designer ? data.designer : null,
        ":category": data.category ? data.category : null,
        ":itemCondition": data.itemCondition ? data.itemCondition : null,
        ":receipt": data.receipt ? data.receipt : null,
        ":tags": data.tags ? data.tags : null,
        ":images": data.images ? data.images : null,
        ":submissionId": data.submissionId ? data.submissionId : null,
        ":submitted": data.submitted ? data.submitted : null,
        ":submissionDate": data.submissionDate ? data.submissionDate : null
    },
    ReturnValues: "ALL_NEW"
  };

  try {
    const result = await dynamoDbLib.call("update", params);
    callback(null, success({ status: true }));
  } catch (e) {
    console.log(e);
    callback(null, failure({ status: false }));
  }
}
