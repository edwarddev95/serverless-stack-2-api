import uuid from "uuid";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
    const data = JSON.parse(event.body);
    const params = {
        TableName: process.env.tableName,
        Item: {
            userId: event.requestContext.identity.cognitoIdentityId,
            itemId: uuid.v1(),
            itemType: data.itemType,
            designer: data.designer,
            category: data.category,
            itemCondition: data.itemCondition,
            receipt: data.receipt,
            tags: data.receipt,
            createdAt: Date.now(),
            images: data.images,
            submissionId: data.submissionId,
            submitted: data.submitted,
            submissionDate: data.submissionDate
        }
    };

    try {
        await dynamoDbLib.call("put", params);
        callback(null, success(params.Item));
    } catch (e) {
        callback(null, failure({ status: false }));
    }
}